# Cistercian Numerals Converter

A Python 3 script with Tkinter GUI to convert numbers from 0 to 9999 to Cistercian numerals.

An "Extended mode" to add parts to the symbols so they can show two more digits without disturbing the original numerals is designed (Just a humble idea to extend the range haha)

Upcoming changes:

- [ ] Save to image file (PNG)

- [ ] Extended mode

- [ ] Batch/Multi mode

