from turtle import *
from tkinter import *



unit=1618
magnification = 0.2
phi = 1.61803
sin45 = 0.707106

drawUnit = unit*magnification
bWing = drawUnit/phi
sWing = bWing/2 #drawUnit*(1 - (1/phi)) #Older Version

hideturtle()#Initialize Turtle before Tkinter

def relTurn(place, angle, heading):
	
	#Because turning left or right depends on whether we wanna do something on the left or right side of the numerals, this function determines that based on the place/position of the specific number that we are dealing with.
	#"place" pertains to the position of the particular number in the desired input number from 0 to 3 left to right 
	if heading == "fd": #"fd" and "bk" do not mean forward and backwards within this function and all calls made to it; "fd" simply means the way the turtle goes about drawing whatever number when we are drawing the number 1 on the upper right part of the numeral while going "forwards", and every other turn can be extrapolated from that.
		
		if place == 2 or place == 1 : 
				lt(angle)
		else :
				rt(angle)
				
	else :  #i.e "bk"
		if place == 2 or place == 1 :
			rt(angle)
		else :
			lt(angle)


def drawNum(place, value): #This function contains the commands to draw the different parts of the numerals pertaining to different numbers.
	
	if value == 0:
		pass
		
	elif value == 1:
		
		relTurn(place, 90, "fd")
		fd(sWing)
		bk(sWing)
		relTurn(place, 90, "bk")
		
	
	elif value == 2:
		
		bk(sWing)
		relTurn(place, 90, "fd")
		fd(sWing)
		bk(sWing)
		relTurn(place, 90, "bk")
		fd(sWing)
		
	elif value == 3 :
		relTurn(place, 135, "fd")
		fd(sWing/sin45)
		bk(sWing/sin45)
		relTurn(place, 135, "bk")
				
	
	elif value == 4 :
		
		bk(sWing)
		relTurn(place, 45, "fd")
		fd(sWing/sin45)
		bk(sWing/sin45)
		relTurn(place, 45, "bk")
		fd(sWing)
		
	elif value == 5 :
		
		relTurn(place, 90, "fd")
		fd(sWing)
		relTurn(place, 135, "fd")
		fd(sWing/sin45)
		relTurn(place, 135, "fd")
		fd(sWing)
		
	elif value == 6 :
		relTurn(place, 90, "fd")
		penup()
		fd(sWing)
		pendown()
		relTurn(place, 90, "fd")
		fd(sWing)
		penup()
		relTurn(place, 90, "fd")
		fd(sWing)
		relTurn(place, 90, "fd")
		fd(sWing)
		pendown()
		
		
	elif value == 7 :
		
		relTurn(place, 90, "fd")
		fd(sWing)
		relTurn(place, 90, "fd")
		fd(sWing)
		penup()
		relTurn(place, 90, "fd")
		fd(sWing)
		relTurn(place, 90, "fd")
		fd(sWing)
		pendown()
		
	elif value == 8 :
		
		bk(sWing)
		relTurn(place, 90, "fd")
		fd(sWing)
		relTurn(place, 90, "bk")
		fd(sWing)
		penup()
		relTurn(place, 90, "bk")
		fd(sWing)
		relTurn(place, 90, "fd")
		pendown()
		
	elif value == 9 :
		
		relTurn(place, 90, "fd")
		fd(sWing)
		relTurn(place, 90, "fd")
		fd(sWing)
		relTurn(place, 90, "fd")
		fd(sWing)
		relTurn(place, 90, "fd")
		fd(sWing)
		
	else:
		print("Uhhhh, how did this happen? lol")
				
		

def draw():
	
	global number
	#number=str(randint(0,9999)) #for random generation. Put it in a loop with a reset() and stuff, it's cool.
	reset()
	hideturtle()
	speed(0)
	pensize(10)
	
	lt(90)
	bk(drawUnit)
	fd(drawUnit) #The root
	
	

	number = number.rjust(4,"0")
	print(number)
	
	drawNum(3, int(number[3]))
	drawNum(2, int(number[2]))
	rt(180)
	fd(drawUnit)
	drawNum(1, int(number[1]))
	drawNum(0, int(number[0]))
	
	
	
window = Tk()
window.title("Cistercian Numeral Converter")
window.geometry('644x400')

lbl = Label(window, text="Number:")
lbl.grid(column=0, row=0)
txt = Entry(window,width=5)
txt.grid(column=1, row=0)


def buttClick():
	global number
	number = txt.get()
	draw()

btn = Button(window, text="Draw", command=buttClick)

btn.grid(column=2, row=0)
window.mainloop()
done()
		
			
